let fibonacci n =
    if n < 0 then -1
    else
        let rec fibonacci_aux n tmp acc =
            if n == 0 then acc
            else fibonacci_aux (n - 1) acc (tmp + acc)
        in
        fibonacci_aux n 1 0

let () =
    print_string "Test fibonacci (-42): ";
    print_int (fibonacci (-42));
    print_char '\n';
    print_string "Test fibonacci 1: ";
    print_int (fibonacci 1);
    print_char '\n';
    print_string "Test fibonacci 3: ";
    print_int (fibonacci 3);
    print_char '\n';
    print_string "Test fibonacci 6: ";
    print_int (fibonacci 6);
    print_char '\n'
