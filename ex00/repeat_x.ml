let repeat_x num =
    if num < 0 then "Error"
    else
        let rec concat str n =
            if n == 0 then
                str
            else
                let str = str ^ "x" in
                concat str (n - 1)
        in
        concat "" num

let () =
    print_string "Test repeat_x (-1): ";
    print_endline (repeat_x (-1));
    print_string "Test repeat_x 0: ";
    print_endline (repeat_x 0);
    print_string "Test repeat_x 1: ";
    print_endline (repeat_x 1);
    print_string "Test repeat_x 2: ";
    print_endline (repeat_x 2);
    print_string "Test repeat_x 5: ";
    print_endline (repeat_x 5)
