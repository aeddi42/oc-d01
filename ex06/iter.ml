let rec iter func acc count =
    if count < 0 then -1
    else if count == 0 then acc
    else func (iter func acc (count - 1))

let () =
    print_string "Test iter (fun x -> x * x) 2 4: ";
    print_int (iter (fun x -> x * x) 2 4);
    print_char '\n';
    print_string "Test iter (fun x -> x * 2) 2 4: ";
    print_int (iter (fun x -> x * 2) 2 4);
    print_char '\n'
