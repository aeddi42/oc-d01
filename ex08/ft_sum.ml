let ft_sum func low up =
    if up < low then nan
    else
        let rec ft_sum_aux cur acc =
            if cur > up then acc
            else ft_sum_aux (cur + 1) ((func cur) +. acc)
        in
        ft_sum_aux low 0.0

let () =
    print_string "Test ft_sum (fun i -> float_of_int (i * i)) 1 10: ";
    print_float (ft_sum (fun i -> float_of_int (i * i)) 1 10);
    print_char '\n';
    print_string "Test ft_sum (fun i -> float_of_int (i * i)) 11 10: ";
    print_float (ft_sum (fun i -> float_of_int (i * i)) 11 10);
    print_char '\n'
