let repeat_string ?(str="x") num =
    if num < 0 then "Error"
    else
        let rec concat str2 n =
            if n == 0 then
                str2
            else
                let str2 = str2 ^ str in
                concat str2 (n - 1)
        in
        concat "" num

let () =
    print_string "Test repeat_string (-1): ";
    print_endline (repeat_string (-1));
    print_string "Test repeat_string 0: ";
    print_endline (repeat_string 0);
    print_string "Test repeat_string ~str:\"Toto\" 1: ";
    print_endline (repeat_string ~str:"Toto" 1);
    print_string "Test repeat_string 2: ";
    print_endline (repeat_string 2);
    print_string "Test repeat_string ~str:\"a\" 5: ";
    print_endline (repeat_string ~str:"a" 5);
    print_string "Test repeat_string ~str:\"what\" 3: ";
    print_endline (repeat_string ~str:"what" 3)
