let rec converges func value iter =
    if iter < 0 then false
    else if (func value) == value then true
    else converges func (func value) (iter - 1)

let () =
    print_string "Test converges (( * ) 2) 2 5: ";
    print_endline (if (converges (( * ) 2) 2 5) then "true" else "false");
    print_string "Test converges (fun x -> x / 2) 2 3: ";
    print_endline (if (converges (fun x -> x / 2) 2 3) then "true" else "false");
    print_string "Test converges (fun x -> x / 2) 2 2: ";
    print_endline (if (converges (fun x -> x / 2) 2 2) then "true" else "false")
