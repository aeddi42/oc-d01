let leibniz_pi delta =
    if delta < 0.0 then -1
    else
        let ref = 4. *. atan 1. in
        let abs x = if x < 0. then (-.x) else x in
        let rec iter value count =
            if value > 0. && (abs (ref -. (4. *. value))) < delta then count
            else iter (value +. (((-1.) ** (float_of_int count)) /. (float_of_int ((2 * count) + 1)))) (count + 1)
        in
        iter 0. 0

let () =
    print_string "leibniz_pi 0.1 = ";
    print_int (leibniz_pi 0.1);
    print_char '\n';
    print_string "leibniz_pi 0.042 = ";
    print_int (leibniz_pi 0.042);
    print_char '\n';
    print_string "leibniz_pi 0.0000084 = ";
    print_int (leibniz_pi 0.0000084);
    print_char '\n'
